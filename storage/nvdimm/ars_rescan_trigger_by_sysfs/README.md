# storage/nvdimm/ars_rescan_trigger_by_sysfs

Storage: feature test for ars_rescan trigger by sysfs

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
